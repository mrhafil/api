<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/key', function() {
    return str_random(32);
});

$router->get('/foo', function() {
    return 'Hello, GET Method';
});

$router->post('/bar', function() {
    return 'Hello, Aku Hafil';
});

// $router->get('/get', function () {
//     return 'GET';
// });

// $router->post('/post', function () {
//     return 'POST';
// });

// $router->put('/put', function () {
//     return 'PUT';
// });

// $router->patch('/patch', function () {
//     return 'PATCH';
// });

// $router->delete('/delete', function () {
//     return 'DELETE';
// });

// $router->options('/options', function () {
//     return 'OPTIONS';
// });

// $router->get('/user/id', function($id) {
//     return 'User Id =' . $id;
// });

// $router->group(['prefix' => 'api'], function($router) {
//     $router->get('products', 'ProductController@showAllProducts');
//     $router->get('products/{product_id}', 'ProductController@showOneProducts');
//     $router->post('products', 'ProductController@create');
// });

// $router->group(['prefix' => 'api'], function($router) {
//     $router->get('users', 'UserController@showAllUsers');
// });

// $router->get('/product', 'ProductController@index');
// $router->get('/product/{product_id}', 'ProductController@detail');
// $router->post('/product/save', 'ProductController@store');
// $router->post('/product/{product_id}/update', 'ProductController@update');
// $router->post('/product/{product_id}/delete', 'ProductController@destroy');

//Products
$router->get("products", "ProductController@index");
$router->get("products/{product_id}", "ProductController@detail");
$router->post("products/insert", "ProductController@tambah");
$router->patch("products/update", "ProductController@edit");
$router->delete("products/delete/{product_id}", "ProductController@hapus");

//Users
$router->get("users", "UserController@index");
$router->get("users/{user_id}", "UserController@detail");
$router->post("users/insert", "UserController@tambah");
$router->patch("users/update", "UserController@edit");
$router->delete("users/delete/{user_id}", "UserController@hapus");

//Category
$router->get("categories", "CategoriesController@index");
$router->get("categories/{category_id}", "CategoriesController@detail");
$router->post("categories/insert", "CategoriesController@tambah");
$router->patch("categories/update", "CategoriesController@edit");
$router->delete("categories/delete/{category_id}", "CategoriesController@hapus");

//Stock
$router->get("stock", "StockController@index");
$router->get("stock/{stock_id}", "StockController@detail");
$router->post("stock/insert", "StockController@tambah");
$router->patch("stock/update", "StockController@edit");
$router->delete("stock/delete/{stock_id}", "StockController@hapus");

//UserDetails
$router->get("user_details", "UDController@index");
$router->get("user_details/{ud_id}", "UDController@detail");
$router->post("user_details/insert", "UDController@tambah");
$router->patch("user_details/update", "UDController@edit");
$router->delete("user_details/delete/{ud_id}", "UDController@hapus");

//Transaksi
$router->get("transactions", "TransController@index");
$router->get("transactions/{transaction_id}", "TransController@detail");
$router->post("transactions/insert", "TransController@tambah");
$router->patch("transactions/update", "TransController@edit");
$router->delete("transactions/delete/{transaction_id}", "TransController@hapus");

//TransaksiDetails
$router->get("transaction_details", "TransDetailsController@index");
$router->get("transaction_details/{td_id}", "TransDetailsController@detail");
$router->post("transaction_details/insert", "TransDetailsController@tambah");
$router->patch("transaction_details/update", "TransDetailsController@edit");
$router->delete("transaction_details/delete/{td_id}", "TransDetailsController@hapus");

//Images
$router->get("images", "ImagesController@index");
$router->get("images/{images_id}", "ImagesController@detail");
$router->post("images/insert", "ImagesController@tambah");
$router->post("images/update", "ImagesController@edit");
$router->delete("images/delete/{images_id}", "ImagesController@hapus");

//Login&Register
$router->post('/register', 'AuthController@register');
$router->post('/login', 'AuthController@login');
$router->get('/user', 'UserRegisterController@index');

$router->get('/posts/get-request-json', 'PostsController@getRequestJson');

// $router->get("", function () use ($router) {
//     $res['success'] = true;
//     $res['result'] = "Hello there welcome to web api using lumen tutorial!";
//     return response($res);
//   });
//   $router->post("login", "LoginController@index");
//   $router->post("register", "UserRegisterController@register");
//   $router->get("users/{user_id}", ['middleware' => 'auth', 'uses' =>  'UserController@get_user']);

