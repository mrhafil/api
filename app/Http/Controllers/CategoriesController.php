<?php

namespace App\Http\Controllers;

use DB;
use App\Categories;
use Illuminate\Http\Request;

class CategoriesController extends Controller{

    public function index() {
        $getData = DB::table('categories')->get();

        $out = [
          "messsage" => "list_categories",
          "results" => $getData
        ];

        return response()->json($out, 200);
    }
       public function detail($category_id) {        
           $getData = DB::table('categories')
                        ->select(DB::raw('category_id, user_id, category_name, category_info'))          
                        ->where('category_id', $category_id)
                        ->get();
    
           $out = [
               "message" => "detail_category",
               "results" => $getData
           ];
    
           return response()->json($out, 200);
       }
   
       public function tambah(Request $request) {
           if ($request->isMethod('post')) {
    
               $this->validate($request, [
                   //'product_id' => 'required',
                //    'category_id' => 'required',
                   'user_id' => 'required',
                   'category_name' => 'required',
                   'category_info' => 'required',
               ]);
   
               //$product_id = $request-> input('product_id'); 
            //    $category_id = $request->input('category_id');
               $user_id = $request->input('user_id');
               $category_name = $request->input('category_name');
               $category_info = $request->input('category_info');
               
    
               $data = [
                   //'product_id' => $product_id,
                //    'category_id' => $category_id,
                   'user_id' => $user_id,
                   'category_name' => $category_name,
                   'category_info' => $category_info,
               ];
   
               $insert = DB::table('categories')->insert($data);
    
               if ($insert) {
                   $out  = [
                       "message" => "berhasil_tambah_data",
                       "results" => $data,
                       "code"    => 200,
                   ];
               } else {
                   $out  = [
                       "message" => "gagal_tambah_data",
                       "results" => $data,
                       "code"    => 404,
                   ];
               }
    
               return response()->json($out, $out['code']);
           }
       }
   
       public function edit(Request $request) { 
           if ($request->isMethod('patch')) {
    
               $this->validate($request, [
                'category_id' => 'required',
                'user_id' => 'required',
                'category_name' => 'required',
                'category_info' => 'required',
               ]);
               
               $category_id = $request->input('category_id');
               $user_id = $request->input('user_id');
               $category_name = $request->input('category_name');
               $category_info = $request->input('category_info');

               $patch = DB::table('categories')->where('category_id', $category_id);
    
               $data = [
                'category_id' => $category_id,
                'user_id' => $user_id,
                'category_name' => $category_name,
                'category_info' => $category_info,
               ];
    
               $update = $patch->update($data);
    
               if ($update) {
                   $out  = [
                       "message" => "berhasil_update_data",
                       "results" => $data,
                       "code"    => 200,
                   ];
               } else {
                   $out  = [
                       "message" => "gagal_update_data",
                       "results" => $data,
                       "code"   => 404,
                   ];
               }
    
               return response()->json($out, $out['code']);
           }
       }
       
       public function hapus($category_id) {
           $hapus = DB::table('categories')->where('category_id', $category_id);
           
           if (!$hapus) {
               $data = [
                   "message" => "id_tidak_ditemukan",
               ];
           } else {
               $hapus->delete();
               $data = [
                   "message" => "berhasil_hapus_data"
               ];
           }
    
           return response()->json($data, 200);
       }
}