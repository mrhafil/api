<?php

namespace App\Http\Controllers;

use DB;
use App\Stock;
use Illuminate\Http\Request;

class StockController extends Controller{

    public function index() {
        $getData = DB::table('stock')->get();

        $out = [
          "messsage" => "list_stock",
          "results" => $getData
        ];

        return response()->json($out, 200);
    }
       public function detail($stock_id) {        
           $getData = DB::table('stock')
                        ->select(DB::raw('stock_id, product_id, stock_size, stock_color, stock_weight, stock_price, stock_qty'))          
                        ->where('stock_id', $stock_id)
                        ->get();
    
           $out = [
               "message" => "detail_stock",
               "results" => $getData
           ];
    
           return response()->json($out, 200);
       }
   
       public function tambah(Request $request) {
           if ($request->isMethod('post')) {
    
               $this->validate($request, [
                // 'stock_id' => 'required',
                   'product_id' => 'required',
                   'stock_size' => 'required',
                   'stock_color' => 'required',
                   'stock_weight' => 'required',
                   'stock_price' => 'required',
                   'stock_qty' => 'required',
               ]);

            //    $stock_id = $request->input('category_id');
               $product_id = $request-> input('product_id'); 
               $stock_size = $request->input('stock_size');
               $stock_color = $request->input('stock_color');
               $stock_weight = $request->input('stock_weight');
               $stock_price = $request->input('stock_price');
               $stock_qty = $request->input('stock_qty');
    
               $data = [
                // 'stock_id' => $stock_id,
                   'product_id' => $product_id,
                   'stock_size' => $stock_size,
                   'stock_color' => $stock_color,
                   'stock_weight' => $stock_weight,
                   'stock_price' => $stock_price,
                   'stock_qty' => $stock_qty,
               ];
   
               $insert = DB::table('stock')->insert($data);
    
               if ($insert) {
                   $out  = [
                       "message" => "berhasil_tambah_data",
                       "results" => $data,
                       "code"    => 200,
                   ];
               } else {
                   $out  = [
                       "message" => "gagal_tambah_data",
                       "results" => $data,
                       "code"    => 404,
                   ];
               }
    
               return response()->json($out, $out['code']);
           }
       }
   
       public function edit(Request $request) { 
           if ($request->isMethod('patch')) {
    
               $this->validate($request, [
                'stock_id' => 'required',
                'product_id' => 'required',
                'stock_size' => 'required',
                'stock_color' => 'required',
                'stock_weight' => 'required',
                'stock_price' => 'required',
                'stock_qty' => 'required',
               ]);

               $stock_id = $request->input('stock_id');
               $product_id = $request-> input('product_id'); 
               $stock_size = $request->input('stock_size');
               $stock_color = $request->input('stock_color');
               $stock_weight = $request->input('stock_weight');
               $stock_price = $request->input('stock_price');
               $stock_qty = $request->input('stock_qty');

               $patch = DB::table('stock')->where('stock_id', $stock_id);
    
               $data = [
                'stock_id' => $stock_id,
                'product_id' => $product_id,
                'stock_size' => $stock_size,
                'stock_color' => $stock_color,
                'stock_weight' => $stock_weight,
                'stock_price' => $stock_price,
                'stock_qty' => $stock_qty,
               ];
    
               $update = $patch->update($data);
    
               if ($update) {
                   $out  = [
                       "message" => "berhasil_update_data",
                       "results" => $data,
                       "code"    => 200,
                   ];
               } else {
                   $out  = [
                       "message" => "gagal_update_data",
                       "results" => $data,
                       "code"   => 404,
                   ];
               }
    
               return response()->json($out, $out['code']);
           }
       }
       
       public function hapus($stock_id) {
           $hapus = DB::table('stock')->where('stock_id', $stock_id);
           
           if (!$hapus) {
               $data = [
                   "message" => "id_tidak_ditemukan",
               ];
           } else {
               $hapus->delete();
               $data = [
                   "message" => "berhasil_hapus_data"
               ];
           }
    
           return response()->json($data, 200);
       }
}