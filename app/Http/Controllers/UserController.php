<?php

namespace App\Http\Controllers;

use DB;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware("login");
    // }
 
    // public function get_index()
    // {
    //     return "Anda Berhasil masuk";
    // }

    public function index() {
        $getData = DB::table('users')->get();

        $out = [
          "messsage" => "list_users",
          "results" => $getData
        ];

        return response()->json($out, 200);
   }
       public function detail($user_id) {        
           $getData = DB::table('users')
                        ->select(DB::raw('user_id, user_email, user_password, user_phone, user_phone_verf, created_at, updated_at, user_token, user_class, user_role, user_status'))       
                        ->where('user_id', $user_id)
                        ->get();
    
           $out = [
               "message" => "detail_userss",
               "results" => $getData
           ];
    
           return response()->json($out, 200);
       }
   
       public function tambah(Request $request) {
           if ($request->isMethod('post')) {
    
               $this->validate($request, [
                   //'product_id' => 'required',
                   'user_email' => 'required',
                   'user_password' => 'required',
                   'user_phone' => 'required',
                   'user_phone_verf' => 'required',
                   'created_at' => 'required',
                   'updated_at' => 'required',
                   'user_token' => 'required',
                   'user_class' => 'required',
                   'user_role' => 'required',
                   'user_status' => 'required',

               ]);
   
               //$product_id = $request-> input('product_id'); 
               $user_email = $request->input('user_email');
               $user_password = $request->input('user_password');
               $user_phone = $request->input('user_phone');
               $user_phone_verf = $request->input('user_phone_verf');
               $created_at = $request->input('created_at');
               $updated_at = $request->input('updated_at');
               $user_token = $request->input('user_token');
               $user_class = $request->input('user_class');
               $user_role = $request->input('user_role');
               $user_status = $request->input('user_status');
    
               $data = [
                   //'product_id' => $product_id,
                   'user_email' => $user_email,
                   'user_password' => $user_password,
                   'user_phone' => $user_phone,
                   'user_phone_verf' => $user_phone_verf,
                   'created_at' => $created_at,
                   'updated_at' => $updated_at,
                   'user_token' => $user_token,
                   'user_class' => $user_class,
                   'user_role' => $user_role,
                   'user_status' => $user_status,
               ];
   
               $insert = DB::table('users')->insert($data);
    
               if ($insert) {
                   $out  = [
                       "message" => "berhasil_tambah_data",
                       "results" => $data,
                       "code"    => 200,
                   ];
               } else {
                   $out  = [
                       "message" => "gagal_tambah_data",
                       "results" => $data,
                       "code"    => 404,
                   ];
               }
    
               return response()->json($out, $out['code']);
           }
       }
   
       public function edit(Request $request) { 
           if ($request->isMethod('patch')) {
    
               $this->validate($request, [
                'user_id' => 'required',
                'user_email' => 'required',
                'user_password' => 'required',
                'user_phone' => 'required',
                'user_phone_verf' => 'required',
                'created_at' => 'required',
                'updated_at' => 'required',
                'user_token' => 'required',
                'user_class' => 'required',
                'user_role' => 'required',
                'user_status' => 'required',
               ]);

               $user_id = $request->input('user_id');
               $user_email = $request->input('user_email');
               $user_password = $request->input('user_password');
               $user_phone = $request->input('user_phone');
               $user_phone_verf = $request->input('user_phone_verf');
               $created_at = $request->input('created_at');
               $updated_at = $request->input('updated_at');
               $user_token = $request->input('user_token');
               $user_class = $request->input('user_class');
               $user_role = $request->input('user_role');
               $user_status = $request->input('user_status');

               $patch = DB::table('users')->where('user_id', $user_id);
    
               $data = [
                'user_id' => $user_id,
                'user_email' => $user_email,
                'user_password' => $user_password,
                'user_phone' => $user_phone,
                'user_phone_verf' => $user_phone_verf,
                'created_at' => $created_at,
                'updated_at' => $updated_at,
                'user_token' => $user_token,
                'user_class' => $user_class,
                'user_role' => $user_role,
                'user_status' => $user_status,
               ];
    
               $update = $patch->update($data);
    
               if ($update) {
                   $out  = [
                       "message" => "berhasil_update_data",
                       "results" => $data,
                       "code"    => 200,
                   ];
               } else {
                   $out  = [
                       "message" => "gagal_update_data",
                       "results" => $data,
                       "code"   => 404,
                   ];
               }
    
               return response()->json($out, $out['code']);
           }
       }
       
       public function hapus($user_id) {
           $hapus = DB::table('users')->where('user_id', $user_id);
           
           if (!$hapus) {
               $data = [
                   "message" => "id_tidak_ditemukan",
               ];
           } else {
               $hapus->delete();
               $data = [
                   "message" => "berhasil_hapus_data"
               ];
           }
    
           return response()->json($data, 200);
       }   
}