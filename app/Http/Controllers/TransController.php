<?php

namespace App\Http\Controllers;

use DB;
use App\Transactions;
use Illuminate\Http\Request;

class TransController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware("login");
    // }
 
    // public function get_index()
    // {
    //     return "Anda Berhasil masuk";
    // }

    public function index() {
        $getData = DB::table('transactions')->get();

        $out = [
          "messsage" => "list_users",
          "results" => $getData
        ];

        return response()->json($out, 200);
   }
       public function detail($transaction_id) {        
           $getData = DB::table('transactions')
                        ->select(DB::raw('transaction_id, user_id, transaction_date, transaction_expedition, transaction_status'))       
                        ->where('transaction_id', $transaction_id)
                        ->get();
    
           $out = [
               "message" => "detail_transaksi",
               "results" => $getData
           ];
    
           return response()->json($out, 200);
       }
   
       public function tambah(Request $request) {
           if ($request->isMethod('post')) {
    
               $this->validate($request, [
                   //'product_id' => 'required',
                //    'ud_id' => 'required',
                // 'transaction_id' => 'required',
                'user_id' => 'required',
                'transaction_date' => 'required',
                'transaction_expedition' => 'required', 
                'transaction_status' => 'required',
               ]);
   
               //$product_id = $request-> input('product_id'); 
            //    $transaction_id = $request->input('$transaction_id');
               $user_id = $request->input('user_id');
               $transaction_date = $request->input('transaction_date');
               $transaction_expedition = $request->input('transaction_expedition');
               $transaction_status = $request->input('transaction_status');

               $data = [
                   //'product_id' => $product_id,
                //    'ud_id' => $ud_id,
                // 'transaction_id' => $transaction_id,
                'user_id' => $user_id,
                'transaction_date' => $transaction_date,
                'transaction_expedition' => $transaction_expedition, 
                'transaction_status' => $transaction_status,
               ];
   
               $insert = DB::table('transactions')->insert($data);
    
               if ($insert) {
                   $out  = [
                       "message" => "berhasil_tambah_data",
                       "results" => $data,
                       "code"    => 200,
                   ];
               } else {
                   $out  = [
                       "message" => "gagal_tambah_data",
                       "results" => $data,
                       "code"    => 404,
                   ];
               }
    
               return response()->json($out, $out['code']);
           }
       }
   
       public function edit(Request $request) { 
           if ($request->isMethod('patch')) {
    
               $this->validate($request, [
                'transaction_id' => 'required',
                'user_id' => 'required',
                'transaction_date' => 'required',
                'transaction_expedition' => 'required', 
                'transaction_status' => 'required'
               ]);

               $transaction_id = $request->input('transaction_id');
               $user_id = $request->input('user_id');
               $transaction_date = $request->input('transaction_date');
               $transaction_expedition = $request->input('transaction_expedition');
               $transaction_status = $request->input('transaction_status');

               $patch = DB::table('transactions')->where('transaction_id', $transaction_id);
    
               $data = [
                'transaction_id' => $transaction_id,
                'user_id' => $user_id,
                'transaction_date' => $transaction_date,
                'transaction_expedition' => $transaction_expedition, 
                'transaction_status' => $transaction_status,
            ];
    
               $update = $patch->update($data);
    
               if ($update) {
                   $out  = [
                       "message" => "berhasil_update_data",
                       "results" => $data,
                       "code"    => 200,
                   ];
               } else {
                   $out  = [
                       "message" => "gagal_update_data",
                       "results" => $data,
                       "code"   => 404,
                   ];
               }
    
               return response()->json($out, $out['code']);
           }
       }
       
       public function hapus($transaction_id) {
        $hapus = DB::table('transactions')->where('transaction_id', $transaction_id);
           
           if (!$hapus) {
               $data = [
                   "message" => "id_tidak_ditemukan",
               ];
           } else {
               $hapus->delete();
               $data = [
                   "message" => "berhasil_hapus_data"
               ];
           }
    
           return response()->json($data, 200);
       }   
}