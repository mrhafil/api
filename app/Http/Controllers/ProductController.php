<?php

namespace App\Http\Controllers;

use DB;
use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
//     public function __construct()
//     {
        
//     }

     public function index() {
          $getData = DB::table('products')->get();

          $out = [
            "messsage" => "list_products",
            "results" => $getData
          ];

          return response()->json($out, 200);
     }
         public function detail($product_id) {        
             $getData = DB::table('products')
                          ->select(DB::raw('product_id, category_id, product_name, product_brand, product_info'))          
                          ->where('product_id', $product_id)
                          ->get();
      
             $out = [
                 "message" => "detail_products",
                 "results" => $getData
             ];
      
             return response()->json($out, 200);
         }
     
         public function tambah(Request $request) {
             if ($request->isMethod('post')) {
      
                 $this->validate($request, [
                     //'product_id' => 'required',
                     'category_id' => 'required',
                     'product_name' => 'required',
                     'product_brand' => 'required',
                     'product_info' => 'required',
                 ]);
     
                 //$product_id = $request-> input('product_id'); 
                 $category_id = $request->input('category_id');
                 $product_name = $request->input('product_name');
                 $product_brand = $request->input('product_brand');
                 $product_info = $request->input('product_info');
                 
      
                 $data = [
                     //'product_id' => $product_id,
                     'category_id' => $category_id,
                     'product_name' => $product_name,
                     'product_brand' => $product_brand,
                     'product_info' => $product_info,
                 ];
     
                 $insert = DB::table('products')->insert($data);
      
                 if ($insert) {
                     $out  = [
                         "message" => "berhasil_tambah_data",
                         "results" => $data,
                         "code"    => 200,
                     ];
                 } else {
                     $out  = [
                         "message" => "gagal_tambah_data",
                         "results" => $data,
                         "code"    => 404,
                     ];
                 }
      
                 return response()->json($out, $out['code']);
             }
         }
     
         public function edit(Request $request) { 
             if ($request->isMethod('patch')) {
      
                 $this->validate($request, [
                     'product_id' => 'required',
                     'category_id' => 'required',
                     'product_name' => 'required',
                     'product_brand' => 'required',
                     'product_info' => 'required'
                 ]);
     
                 $product_id = $request->input('product_id');
                 $category_id = $request->input('category_id');
                 $product_name = $request->input('product_name');
                 $product_brand = $request->input('product_brand');
                 $product_info = $request->input('product_info');
     
                 $patch = DB::table('products')->where('product_id', $product_id);
      
                 $data = [
                     'category_id' => $category_id,
                     'product_name' => $product_name,
                     'product_brand' => $product_brand,
                     'product_info' => $product_info,
                 ];
      
                 $update = $patch->update($data);
      
                 if ($update) {
                     $out  = [
                         "message" => "berhasil_update_data",
                         "results" => $data,
                         "code"    => 200,
                     ];
                 } else {
                     $out  = [
                         "message" => "gagal_update_data",
                         "results" => $data,
                         "code"   => 404,
                     ];
                 }
      
                 return response()->json($out, $out['code']);
             }
         }
         
         public function hapus($product_id) {
             $hapus = DB::table('products')->where('product_id', $product_id);
             
             if (!$hapus) {
                 $data = [
                     "message" => "id_tidak_ditemukan",
                 ];
             } else {
                 $hapus->delete();
                 $data = [
                     "message" => "berhasil_hapus_data"
                 ];
             }
      
             return response()->json($data, 200);
         }
     }     

     // public function show($product_id)
     // {
     //   $data = Product::where('product_id',$product_id)->get();
     //   return response ($data);
     // }

     // public function store (Request $request)
     // {
     //    $data = new Product();
     //    $data -> category_id = $request -> input ('category_id');
     //    $data -> product_name = $request -> input ('product_name');
     //    $data -> product_brand = $request -> input ('product_brand');
     //    $data -> product_info = $request -> input ('product_info');
     //    $data -> save ();

     //    return response('Berhasil Tambah Data');
     // }

     // public function update (Request $request,$product_id)
     // {
     //    $data = Product::where('product_id',$product_id) -> first();
     //    $data -> category_id = $request -> input ('category_id');
     //    $data -> product_name = $request -> input ('product_name');
     //    $data -> product_brand = $request -> input ('product_brand');
     //    $data -> product_info = $request -> input ('product_info');
     //    $data -> save ();

     //    return response('Berhasil Merubah Data');
     // }

     // public function destroy ($product_id)
     // {
     //    $data = Product::where('product_id',$product_id) -> first();
     //    $data -> delete();

     //    return response('Berhasil Menghapus Data');
     // }

