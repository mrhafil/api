<?php

namespace App\Http\Controllers;

use DB;
use App\UserDetails;
use Illuminate\Http\Request;

class UDController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware("login");
    // }
 
    // public function get_index()
    // {
    //     return "Anda Berhasil masuk";
    // }

    public function index() {
        $getData = DB::table('user_details')->get();

        $out = [
          "messsage" => "list_users",
          "results" => $getData
        ];

        return response()->json($out, 200);
   }
       public function detail($ud_id) {        
           $getData = DB::table('user_details')
                        ->select(DB::raw('ud_id, user_id, ud_shop_name, ud_name, ud_gender, ud_religi, ud_address, ud_city, ud_country'))       
                        ->where('ud_id', $ud_id)
                        ->get();
    
           $out = [
               "message" => "detail_user_detail",
               "results" => $getData
           ];
    
           return response()->json($out, 200);
       }
   
       public function tambah(Request $request) {
           if ($request->isMethod('post')) {
    
               $this->validate($request, [
                   //'product_id' => 'required',
                //    'ud_id' => 'required',
                   'user_id' => 'required',
                   'ud_shop_name' => 'required',
                   'ud_name' => 'required',
                   'ud_gender' => 'required',
                   'ud_religi' => 'required',
                   'ud_address' => 'required',
                   'ud_city' => 'required',
                   'ud_country' => 'required',
               ]);
   
               //$product_id = $request-> input('product_id'); 
            //    $ud_id = $request->input('ud_id');
               $user_id = $request->input('user_id');
               $ud_shop_name = $request->input('ud_shop_name');
               $ud_name = $request->input('ud_name');
               $ud_gender = $request->input('ud_gender');
               $ud_religi = $request->input('ud_religi');
               $ud_address = $request->input('ud_address');
               $ud_city = $request->input('ud_city');
               $ud_country = $request->input('ud_country');

               $data = [
                   //'product_id' => $product_id,
                //    'ud_id' => $ud_id,
                   'user_id' => $user_id,
                   'ud_shop_name' => $ud_shop_name,
                   'ud_name' => $ud_name,
                   'ud_gender' => $ud_gender,
                   'ud_religi' => $ud_religi,
                   'ud_address' => $ud_address,
                   'ud_city' => $ud_city,
                   'ud_country' => $ud_country,
               ];
   
               $insert = DB::table('user_details')->insert($data);
    
               if ($insert) {
                   $out  = [
                       "message" => "berhasil_tambah_data",
                       "results" => $data,
                       "code"    => 200,
                   ];
               } else {
                   $out  = [
                       "message" => "gagal_tambah_data",
                       "results" => $data,
                       "code"    => 404,
                   ];
               }
    
               return response()->json($out, $out['code']);
           }
       }
   
       public function edit(Request $request) { 
           if ($request->isMethod('patch')) {
    
               $this->validate($request, [
                   'ud_id' => 'required',
                   'user_id' => 'required',
                   'ud_shop_name' => 'required',
                   'ud_name' => 'required',
                   'ud_gender' => 'required',
                   'ud_religi' => 'required',
                   'ud_address' => 'required',
                   'ud_city' => 'required',
                   'ud_country' => 'required',
               ]);

               $ud_id = $request->input('ud_id');
               $user_id = $request->input('user_id');
               $ud_shop_name = $request->input('ud_shop_name');
               $ud_name = $request->input('ud_name');
               $ud_gender = $request->input('ud_gender');
               $ud_religi = $request->input('ud_religi');
               $ud_address = $request->input('ud_address');
               $ud_city = $request->input('ud_city');
               $ud_country = $request->input('ud_country');

               $patch = DB::table('user_details')->where('ud_id', $ud_id);
    
               $data = [
                'ud_id' => $ud_id,
                'user_id' => $user_id,
                'ud_shop_name' => $ud_shop_name,
                'ud_name' => $ud_name,
                'ud_gender' => $ud_gender,
                'ud_religi' => $ud_religi,
                'ud_address' => $ud_address,
                'ud_city' => $ud_city,
                'ud_country' => $ud_country,
            ];
    
               $update = $patch->update($data);
    
               if ($update) {
                   $out  = [
                       "message" => "berhasil_update_data",
                       "results" => $data,
                       "code"    => 200,
                   ];
               } else {
                   $out  = [
                       "message" => "gagal_update_data",
                       "results" => $data,
                       "code"   => 404,
                   ];
               }
    
               return response()->json($out, $out['code']);
           }
       }
       
       public function hapus($ud_id) {
           $hapus = DB::table('user_details')->where('ud_id', $ud_id);
           
           if (!$hapus) {
               $data = [
                   "message" => "id_tidak_ditemukan",
               ];
           } else {
               $hapus->delete();
               $data = [
                   "message" => "berhasil_hapus_data"
               ];
           }
    
           return response()->json($data, 200);
       }   
}