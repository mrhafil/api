<?php

namespace App\Http\Controllers;

use DB;
use File;
use App\Images;
use Illuminate\Http\Request;

class ImagesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware("login");
    // }
 
    // public function get_index()
    // {
    //     return "Anda Berhasil masuk";
    // }

    public function index() {
        $getData = DB::table('images')->get();

        $out = [
          "messsage" => "list_images",
          "results" => $getData
        ];

        return response()->json($out, 200);
   }
       public function detail($images_id) {        
           $getData = DB::table('images')
                        ->select(DB::raw('images_id, image, stock_id, images_name, images_upload'))       
                        ->where('images_id', $images_id)
                        ->get();
    
           $out = [
               "message" => "detail_images",
               "results" => $getData
           ];
    
           return response()->json($out, 200);
       }
   
       public function tambah(Request $request) {
           if ($request->isMethod('post')) {
    
               $this->validate($request, [
                   //'product_id' => 'required',
                //    'td_id' => 'required',
                // 'images_id' => 'required',
                'foto' => 'file',
                'stock_id' => 'required',
                'images_name' => 'required', 
               ]);

               if ($request->hasFile('foto')) {
                   $name = $request->file('foto')->getClientOriginalName();
                   $request->file('foto')->move('uploads',$name);
                   
                   $foto = $name;
                   $stock_id = $request->input('stock_id');
                   $images_name = $request->input('images_name');
                   $url = $request->url('uploads'.'/'.$name);
               } else {
                $foto = 'default.png';
                $stock_id = $request->input('stock_id');
                $images_name = $request->input('images_name');
               }
   
               //$td_id = $request-> input('td_id'); 

               $data = [
                   //'product_id' => $product_id,
                //    'ud_id' => $ud_id,
                // 'td_id' => $td_id,
                'foto' => $foto,
                'stock_id' => $stock_id,
                'images_name' => $images_name,
                'url' => $url,
               ];
   
               $insert = DB::table('images')->insert($data);
    
               if ($insert) {
                   $out  = [
                       "message" => "berhasil_tambah_data",
                       "results" => $data,
                       "code"    => 200,
                   ];
               } else {
                   $out  = [
                       "message" => "gagal_tambah_data",
                       "results" => $data,
                       "code"    => 404,
                   ];
               }
    
               return response()->json($out, $out['code']);
           }
       }
   
       public function edit(Request $request) { 
           if ($request->isMethod('post')) {
    
               $this->validate($request, [
                'images_id' => 'required',
                'stock_id' => 'required',
                'images_name' => 'required',
                'foto' => 'file', 
               ]);

               if ($request->hasFile('foto')) {
                $name = $request->file('foto')->getClientOriginalName();
                $request->file('foto')->move('uploads',$name);

                $images_id = $request->input('images_id');
                $stock_id = $request->input('stock_id');
                $images_name = $request->input('images_name');
                $foto = $name;
                $url = $request->url('uploads'.'/'.$name);
               } else {
                    $images_id = $request->input('images_id');
                    $stock_id = $request->input('stock_id');
                    $images_name = $request->input('images_name');
                    $foto = 'default.png';
               }

               $data = [
                'images_id' => $images_id,
                'stock_id' => $stock_id,
                'images_name' => $images_name,
                'foto' => $foto,
                'url' => $url,            
            ];

               $patch = DB::table('images')->where('images_id', $images_id);

               $update = $patch->update($data);
    
               if ($update) {
                   $out  = [
                       "message" => "berhasil_update_data",
                       "results" => $data,
                       "code"    => 200,
                   ];
               } else {
                   $out  = [
                       "message" => "gagal_update_data",
                       "results" => $data,
                       "code"   => 404,
                   ];
               }
    
               return response()->json($out, $out['code']);
           }
       }
       
       public function hapus($images_id) {
        $hapus = DB::table('images')->where('images_id', $images_id);
           
           if (!$hapus) {
               $data = [
                   "message" => "id_tidak_ditemukan",
               ];
           } else {
               $hapus->delete();
               $data = [
                   "message" => "berhasil_hapus_data"
               ];
           }
    
           return response()->json($data, 200);
       }   
}