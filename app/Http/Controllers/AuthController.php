<?php
 
namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
 
 
class AuthController extends Controller
{
 
    public function register(Request $request)
    {
        $this->validate($request, [
            'user_email' => 'required',
            'user_password' => 'required',
            'user_phone' => 'required',
            'user_phone_verf' => 'required',
            'user_class' => 'required',
            'user_role' => 'required',
            'user_status' => 'required',
            'user_token' => 'required'

        ]);
 
        $user_email = $request->input("user_email");
        $user_password = $request->input("user_password");
        $hashPwd = Hash::make($user_password);

        $user_phone = $request->input("user_phone");
        $user_phone_verf = $request->input("user_phone_verf");
        $user_class = $request->input("user_class");
        $user_role = $request->input("user_role");
        $user_status = $request->input("user_status");
        $user_token = $request->input("user_token");

        $data = [
            "user_email" => $user_email,
            "user_password" => $hashPwd,
            "user_phone" => $user_phone,
            "user_phone_verf" => $user_phone_verf,
            "user_class" => $user_class,
            "user_role" => $user_role,
            "user_status" => $user_status,
            "user_token" => $user_token
        ];
 
        if (User::create($data)) {
            $out = [
                "message" => "register_success",
                "code"    => 201,
            ];
        } else {
            $out = [
                "message" => "vailed_regiser",
                "code"   => 404,
            ];
        }
 
        return response()->json($out, $out['code']);
    }
 
    public function login(Request $request)
    {
        $this->validate($request, [
            'user_email' => 'required',
            'user_password' => 'required'
        ]);
 
        $user_email = $request->input("user_email");
        $user_password = $request->input("user_password");
 
        $Users = User::where("user_email", $user_email)->first();
 
        if (!$Users) {
            $out = [
                "message" => "login_vailed",
                "code"    => 401,
                "result"  => [
                "user_token" => null,
                ]
            ];
            return response()->json($out, $out['code']);
        }
 
        if (Hash::check($user_password, $Users->user_password)) {
            $newtoken  = $this->generateRandomString();
 
            $Users->update([
                'user_token' => $newtoken
            ]);
 
            $out = [
                "message" => "login_success",
                "code"    => 200,
                "result"  => [
                    "user_token" => $newtoken,
                ]
            ];
        } else {
            $out = [
                "message" => "login_vailed",
                "code"    => 401,
                "result"  => [
                    "user_token" => null,
                ]
            ];
        }
 
        return response()->json($out, $out['code']);
    }
 
    function generateRandomString($length = 80)
    {
        $karakkter = '012345678dssd9abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $panjang_karakter = strlen($karakkter);
        $str = '';
        for ($i = 0; $i < $length; $i++) {
            $str .= $karakkter[rand(0, $panjang_karakter - 1)];
        }
        return $str;
    }
}