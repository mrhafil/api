<?php

namespace App\Http\Controllers;

use DB;
use App\TransDetails;
use Illuminate\Http\Request;

class TransDetailsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware("login");
    // }
 
    // public function get_index()
    // {
    //     return "Anda Berhasil masuk";
    // }

    public function index() {
        $getData = DB::table('transaction_details')->get();

        $out = [
          "messsage" => "list_detail_transaksi",
          "results" => $getData
        ];

        return response()->json($out, 200);
   }
       public function detail($td_id) {        
           $getData = DB::table('transaction_details')
                        ->select(DB::raw('td_id, transaction_id, stock_id, td_info, td_qty'))       
                        ->where('td_id', $td_id)
                        ->get();
    
           $out = [
               "message" => "detail_transaksi_detil",
               "results" => $getData
           ];
    
           return response()->json($out, 200);
       }
   
       public function tambah(Request $request) {
           if ($request->isMethod('post')) {
    
               $this->validate($request, [
                   //'product_id' => 'required',
                //    'td_id' => 'required',
                'transaction_id' => 'required',
                'stock_id' => 'required',
                'td_info' => 'required',
                'td_qty' => 'required', 
               ]);
   
               //$td_id = $request-> input('td_id'); 
               $transaction_id = $request->input('transaction_id');
               $stock_id = $request->input('stock_id');
               $td_info = $request->input('td_info');
               $td_qty = $request->input('td_qty');

               $data = [
                   //'product_id' => $product_id,
                //    'ud_id' => $ud_id,
                // 'td_id' => $td_id,
                'transaction_id' => $transaction_id,
                'stock_id' => $stock_id,
                'td_info' => $td_info,
                'td_qty' => $td_qty,
               ];
   
               $insert = DB::table('transaction_details')->insert($data);
    
               if ($insert) {
                   $out  = [
                       "message" => "berhasil_tambah_data",
                       "results" => $data,
                       "code"    => 200,
                   ];
               } else {
                   $out  = [
                       "message" => "gagal_tambah_data",
                       "results" => $data,
                       "code"    => 404,
                   ];
               }
    
               return response()->json($out, $out['code']);
           }
       }
   
       public function edit(Request $request) { 
           if ($request->isMethod('patch')) {
    
               $this->validate($request, [
                'td_id' => 'required',
                'transaction_id' => 'required',
                'stock_id' => 'required',
                'td_info' => 'required',
                'td_qty' => 'required',
               ]);

               $td_id = $request-> input('td_id'); 
               $transaction_id = $request->input('transaction_id');
               $stock_id = $request->input('stock_id');
               $td_info = $request->input('td_info');
               $td_qty = $request->input('td_qty');

               $patch = DB::table('transaction_details')->where('td_id', $td_id);
    
               $data = [
                'td_id' => $td_id,
                'transaction_id' => $transaction_id,
                'stock_id' => $stock_id,
                'td_info' => $td_info,
                'td_qty' => $td_qty,
            ];
    
               $update = $patch->update($data);
    
               if ($update) {
                   $out  = [
                       "message" => "berhasil_update_data",
                       "results" => $data,
                       "code"    => 200,
                   ];
               } else {
                   $out  = [
                       "message" => "gagal_update_data",
                       "results" => $data,
                       "code"   => 404,
                   ];
               }
    
               return response()->json($out, $out['code']);
           }
       }
       
       public function hapus($td_id) {
        $hapus = DB::table('transaction_details')->where('td_id', $td_id);
           
           if (!$hapus) {
               $data = [
                   "message" => "id_tidak_ditemukan",
               ];
           } else {
               $hapus->delete();
               $data = [
                   "message" => "berhasil_hapus_data"
               ];
           }
    
           return response()->json($data, 200);
       }   
}