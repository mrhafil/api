<?php
 
namespace App\Http\Middleware;
 
use Closure;
use App\User;
 
class LoginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->input('user_token')) {
            $check =  User::where('user_token', $request->input('user_token'))->first();
 
            if (!$check) {
                return response('Token Tidak Valid.', 401);
            } else {
                return $next($request);
            }
        } else {
            return response('Silahkan Masukkan Token.', 401);
        }
    }
}